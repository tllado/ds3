#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include <linux/input.h>
#include <linux/joystick.h>

int main (void)
{
	int fd, i;
	int axis[27][2] = {0};
	struct js_event js;

	//open filestream
	if ((fd = open("/dev/input/js0", O_RDONLY)) < 0) {
		perror("jstest");
		return 1;
	}

	while (1) {
		//read filestream
		if (read(fd, &js, sizeof(struct js_event)) != sizeof(struct js_event)) {
			perror("\njstest: error reading");
			return 1;
		}

		//assign readings to array
		if (js.type == 0 || 1)
			axis[js.number-1][js.type]=js.value;

		//display selected elements of array
		printf("%d\t%d\t%d\t%d\t%d\n", js.time, axis[23][0], axis[24][0], axis[25][0], axis[26][0]);

		fflush(stdout);
	}

	printf("jstest: something didn't work");
	return -1;
}